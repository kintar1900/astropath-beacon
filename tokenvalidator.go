package beacon

import (
	"bytes"
	"encoding/json"
	"gitlab.com/kintar1900/astropath-api/client"
	"log"
	"net/http"
)

// Validator checks that a given AuthData is valid
type Validator interface {
	Validate(req client.AuthData) bool
}

const UrlBrov = "https://www.brotherhoodofthevoid.com/api/validate"

// DefaultValidator posts a AuthData to the given URL, expecting 200 for a valid token, and 401 otherwise.
type DefaultValidator struct {
	Url string
}

func (v DefaultValidator) Validate(req client.AuthData) bool {
	bodyBytes, err := json.Marshal(req)
	if err != nil {
		log.Println("error marshalling authdata: ", err)
		return false
	}

	reader := bytes.NewReader(bodyBytes)
	post, err := http.Post(UrlBrov, "application/json", reader)
	if err != nil {
		log.Println("error calling validation endpoint: ", err)
		return false
	}

	defer post.Body.Close()

	return post.StatusCode == 200
}
