package logging

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
)

func SetupLog() {
	rollLogFile()
	logfile, err := os.OpenFile("astropath.log", os.O_CREATE, os.ModePerm)
	if err == nil {
		log.SetOutput(logfile)
	} else {
		log.Println(err)
	}
}

func rollLogFile() {
	lastFile := 0
	existing := false
	err := filepath.Walk("./", func(path string, info os.FileInfo, err error) error {
		if strings.Index(info.Name(), "astropath.log") == 0 {
			lastFile++
			if strings.EqualFold(info.Name(), "astropath.log") {
				existing = true
			}
		}
		return nil
	})
	
	if err == nil {
		if existing {
			err = os.Rename("astropath.log", fmt.Sprintf("astropath.log.%d", lastFile))
			if err != nil {
				panic(fmt.Errorf("could not rename log file: %w", err))
			}
		}
	}
}
