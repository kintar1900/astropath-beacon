package gui

import (
	"github.com/lxn/walk"
	. "github.com/lxn/walk/declarative"
	"gitlab.com/kintar1900/astropath-beacon/data"
	"log"
)

func ShowConfigDialog(owner walk.Form, config *data.Config) (int, error) {
	var dlg *walk.Dialog
	var binder *walk.DataBinder
	var acceptPB, cancelPB *walk.PushButton

	return Dialog{
		AssignTo:      &dlg,
		Title:         "Astropath Beacon Configuration",
		DefaultButton: &acceptPB,
		CancelButton:  &cancelPB,
		DataBinder: DataBinder{
			AssignTo:       &binder,
			Name:           "config",
			DataSource:     config,
			ErrorPresenter: ToolTipErrorPresenter{},
		},
		MinSize: Size{Width: 300, Height: 300},
		Layout:  VBox{},
		Children: []Widget{
			Composite{
				Layout: Grid{Columns: 2},
				Children: []Widget{
					Label{
						Text: "Commander Name:",
					},
					LineEdit{
						Text: Bind("CommanderName"),
					},

					Label{
						Text: "Api Token:",
					},
					LineEdit{Text:
						Bind("ApiToken"),
					},
				},
			},
			Composite{
				Layout: HBox{},
				Children: []Widget{
					HSpacer{},
					PushButton{
						AssignTo: &acceptPB,
						Text:     "OK",
						OnClicked: func() {
							if err := binder.Submit(); err != nil {
								log.Print(err)
								return
							}

							dlg.Accept()
						},
					},
					PushButton{
						AssignTo:  &cancelPB,
						Text:      "Cancel",
						OnClicked: func() { dlg.Cancel() },
					},
				},
			},
		},
	}.Run(owner)
}
