// Astropath - A Discord Bot for Elite : Dangerous squadrons and communities
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package beacon

import (
	"bufio"
	"fmt"
	"github.com/radovskyb/watcher"
	"gitlab.com/kintar1900/astropath-api/client"
	"gitlab.com/kintar1900/astropath-beacon/data"
	"gitlab.com/kintar1900/astropath-beacon/logging"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"regexp"
	"strings"
	"syscall"
	"time"
)

var version string

type AppData struct {
	config data.Config
	api    client.ApiWriter
}

type App interface {
	Run()
}

func NewApp() (App, error) {

	cfg, err := data.LoadConfig()
	if err != nil {
		return nil, err
	}

	return &AppData{
		config: cfg,
	}, nil
}

func (b *AppData) Run() {
	logging.SetupLog()
	fmt.Printf(`
   _____            __                              __  .__     
  /  _  \   _______/  |________  ____ ___________ _/  |_|  |__  
 /  /_\  \ /  ___/\   __\_  __ \/  _ \\____ \__  \\   __\  |  \ 
/    |    \\___ \  |  |  |  | \(  <_> )  |_> > __ \|  | |   Y  \
\____|__  /____  > |__|  |__|   \____/|   __(____  /__| |___|  /
        \/     \/                     |__|       \/          \/`)
	fmt.Printf("\n%64s\n\n", version)

	fmt.Println("Squad beacon starting...")
	log.Println("Astropath ", version, " started.")

	cfg := &b.config
	validateJournalFileLocation(cfg)
	// Validate the configuration
	validateCommanderName(cfg)
	validateApiToken(cfg)

	b.api = client.NewWriter(client.AuthData{
		CommanderName: cfg.CommanderName,
		Token:         cfg.ApiToken,
	})

	// Initialize the file watcher
	go b.watchFile()

	fmt.Printf("Startup complete.  Press CTRL-C to exit.\n")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc
}

func (b *AppData) watchFile() {
	w := watcher.New()
	w.FilterOps(watcher.Create, watcher.Remove, watcher.Write, watcher.Move)

	err := w.Add(b.config.JournalPath)
	if err != nil {
		log.Fatalln(err)
	}

	r := regexp.MustCompile("Journal\\..+\\.log")
	w.AddFilterHook(watcher.RegexFilterHook(r, false))

	reader := &data.DefaultJournalReader{
		EdPath: b.config.JournalPath,
	}

	processRecentJournal(b, reader)

	go func() {
		for {
			select {
			case evt := <-w.Event:
				b.processFileEvent(reader, evt)
			case err := <-w.Error:
				log.Println("Error from file watcher: ", err)
			case <-w.Closed:
				log.Println("Watcher closed.")
				return
			}
		}
	}()

	err = w.Start(1 * time.Second)
	if err != nil {
		log.Fatalln(err)
	}
}

func processRecentJournal(b *AppData, r *data.DefaultJournalReader) {
	var recentFile os.FileInfo
	err := filepath.Walk(b.config.JournalPath, func(path string, info os.FileInfo, err error) error {
		if strings.Index(info.Name(), data.JournalPrefix) != -1 {
			if recentFile == nil {
				recentFile = info
			} else {
				if recentFile.ModTime().Sub(info.ModTime()) < 0 {
					recentFile = info
				}
			}
		}
		return nil
	})
	if err != nil {
		log.Fatalln("error walking journal directory: ", err)
	}

	err = r.SetFile(b.config.JournalPath + string(os.PathSeparator) + recentFile.Name())
	if err != nil {
		log.Fatalln("error setting file: ", err)
	}

	events, err := r.SeekToEnd(true)
	if err != nil {
		log.Fatalln("error reading entries: ", err)
	}

	for _, evt := range events {
		err = b.api.Put(evt)
		if err != nil {
			log.Println(err)
		}
	}
}

func (b *AppData) processFileEvent(r *data.DefaultJournalReader, evt watcher.Event) {
	if evt.IsDir() {
		return
	}

	switch evt.Op {
	case watcher.Create:
		err := r.SetFile(evt.Path)
		if err != nil {
			log.Fatalln(err)
		}
		_, err = r.SeekToEnd(false)
		if err != nil {
			log.Fatalln(err)
		}
		break
	case watcher.Write:
		if !r.HasFile() {
			log.Println("Changing to journal file at ", evt.Path)
			err := r.SetFile(evt.Path)
			if err != nil {
				log.Println("could not switch file: ", err)
			}
		}

		evts, err := r.Next()
		if err != nil {
			log.Fatalln("error reading event: ", err)
		}
		for _, evt := range evts {
			err = b.api.Put(evt)
			if err != nil {
				log.Println("error sending data: ", err)
			}
		}
	}
}

func readLineOrDie() string {
	reader := bufio.NewReader(os.Stdin)
	line, _, err := reader.ReadLine()
	if err != nil {
		ShowErrorAndWait(err)
		os.Exit(0)
	}
	return string(line)
}

func validateApiToken(config *data.Config) {
	fmt.Println("\nValidating your authorization token...")

	if config.ApiToken == "" {
		fmt.Println("Commander, it looks like your token is missing.")
		fmt.Println("Please contact your squad leaders to find out how to get your token.")
		config.ApiToken = ""
		for config.ApiToken == "" {
			fmt.Print("Please enter your API token: ")
			token1 := readLineOrDie()
			fmt.Print("Please enter it again to be sure we have it right: ")
			token2 := readLineOrDie()
			if token1 != token2 {
				fmt.Println("Sorry, commander.  Those tokens don't match.")
			} else {
				validator := DefaultValidator{}
				if !validator.Validate(client.AuthData{
					CommanderName: config.CommanderName,
					Token:         token1,
				}) {
					fmt.Println("That token appears to be invalid.")
					fmt.Println("Be sure you are using your token from the squad website, not Inara or EDDN.")
					fmt.Println("Press 'c' to try again, or 'q' to quit.")
					reader := bufio.NewReader(os.Stdin)

					r, _, err := reader.ReadRune()
					if err != nil {
						ShowErrorAndWait(err)
					}
					switch r {
					case 'q', 'Q':
						os.Exit(0)
					default:
						fmt.Printf("'%s'?  Really?  I'm going to assume you meant 'c'\n", string(r))
						fallthrough
					case 'c', 'C':
						continue
					}
				} else {
					config.ApiToken = token1
				}
			}
		}
	}

	err := data.SaveConfig(config)
	if err != nil {
		ShowErrorAndWait(err)
		os.Exit(0)
	}

	fmt.Printf("\nAPI token valid!  Welcome, commander %s!\n", config.CommanderName)
}

func ShowErrorAndWait(err error) {
	if err == nil {
		return
	}
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Well...that didn't work.  Please tell Kintar the following:")
	fmt.Println(err)
	fmt.Println("\nPress any key to exit...")
	_, _, _ = reader.ReadRune()
}

func validateJournalFileLocation(config *data.Config) {
	fmt.Println("\nLocating journal file...")
	if config.JournalPath != "" {
		return
	}

	path, err := data.GetEdJournalPath()
	if err != nil {
		ShowErrorAndWait(err)
		os.Exit(0)
	}

	config.JournalPath = path

	err = data.SaveConfig(config)
	if err != nil {
		ShowErrorAndWait(err)
		os.Exit(0)
	}
}

func validateCommanderName(config *data.Config) {
	if config.CommanderName == "" {
		fmt.Println("\nSeems we've not met before, commander.")
		fmt.Print("What is your name? ")
		line := readLineOrDie()
		fmt.Println("Nice to meet you!  If you ever want to change your name, tough shit.  That's not implemented yet. ;)")
		config.CommanderName = line
	}

	err := data.SaveConfig(config)
	if err != nil {
		ShowErrorAndWait(err)
		os.Exit(0)
	}

	fmt.Printf("\nStarting up beacon for commander '%s'\n", config.CommanderName)
}
