# Astropath-Beacon

[![current release](https://img.shields.io/badge/dynamic/json?color=blue&label=current%20release&query=%24.latestRelease&url=https%3A%2F%2Fgitlab.com%2Fkintar1900%2Fastropath-beacon%2F-%2Fjobs%2Fartifacts%2Fmaster%2Fraw%2Fbadge-info.json%3Fjob%3DUnitTests&style=flat-square&logo=gitlab)](http://astropath.brotherhoodofthevoid.com/astropath-beacon-latest.zip)

[![master version](https://img.shields.io/badge/dynamic/json?color=blue&label=master%20version&query=%24.masterVersion&url=https%3A%2F%2Fgitlab.com%2Fkintar1900%2Fastropath-beacon%2F-%2Fjobs%2Fartifacts%2Fmaster%2Fraw%2Fbadge-info.json%3Fjob%3DUnitTests&style=flat-square&logo=gitlab)](https://gitlab.com/kintar1900/astropath-beacon) [![GitLab pipeline](https://img.shields.io/gitlab/pipeline/kintar1900/astropath-beacon?style=flat-square&logo=gitlab)](https://gitlab.com/kintar1900/astropath-beacon/builds)

## Overview

Astropath is a suite of tools for Elite Dangerous and Discord to allow squadrons to better coordinate and track the movements of their members.  It consists of an AWS-hosted API and data store ( [here](https://gitlab.com/kintar1900/astropath-api) ), a Windows "beacon" app that feeds ED Journal File information to the API ( this project ), and a Discord bot to process queries into the data ( [here](https://gitlab.com/kintar1900/astropath-bot) ).

## Deployment

The beacon must be built specifically for your squadron.  Your API project should modify the client package to point to your deployed API endpoint in Amazon.  From there, fork this repository and change the module includes for the api client from `"gitlab.com/kintar1900/astropath-api/client"` to instead point at your forked API client code.  From there, the Beacon app will use your client code to connect to your API endpoint.
