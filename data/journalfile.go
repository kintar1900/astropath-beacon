// Astropath - A Discord Bot for Elite : Dangerous squadrons and communities
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package data

import (
	"bufio"
	"errors"
	"fmt"
	"gitlab.com/kintar1900/elite-journalfile/event"
	"os"
)

const (
	edPathSlug    = `Saved Games\Frontier Developments\Elite Dangerous`
	JournalPrefix = "Journal."
)

func GetEdJournalPath() (string, error) {
	userProfileDir := os.Getenv("USERPROFILE")
	if userProfileDir == "" {
		return "", errors.New("could not find USERPROFILE variable")
	} else {
		return fmt.Sprintf("%s\\%s", userProfileDir, edPathSlug), nil
	}
}

type JournalReader interface {
	HasFile() bool
	SetFile(filePath string) error
	SeekToEnd(returnEvents bool) ([]event.Event, error)
	Next() ([]event.Event, error)
}

type DefaultJournalReader struct {
	EdPath      string
	filePath    string
	lastReadPos int64
}

func (d *DefaultJournalReader) HasFile() bool {
	return d.filePath != ""
}

func (d *DefaultJournalReader) SeekToEnd(returnEvents bool) ([]event.Event, error) {
	evts := make([]event.Event, 0)
	var err error
	if returnEvents {
		evts, _, err = event.ParseFile(d.filePath)
		if err != nil {
			return evts, err
		}
	}

	size, err := fileSize(d.filePath)
	d.lastReadPos = size
	return evts, err
}

func fileSize(path string) (int64, error) {
	info, err := os.Stat(path)
	if err != nil {
		return 0, err
	}

	return info.Size(), nil
}

func (d *DefaultJournalReader) Next() ([]event.Event, error) {
	file, err := os.Open(d.filePath)
	if err != nil {
		return nil, err
	}

	_, err = file.Seek(d.lastReadPos, 0)
	if err != nil {
		return nil, err
	}

	defer file.Close()
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	events := make([]event.Event, 0)
	for scanner.Scan() {
		evt, err := event.ParseEvent(scanner.Text())
		if err == nil {
			events = append(events, evt)
		}
	}

	pos, err := fileSize(d.filePath)
	d.lastReadPos = pos
	return events, err
}

func (d *DefaultJournalReader) SetFile(filePath string) error {
	myFile, err := os.Open(filePath)
	if err != nil {
		return err
	}
	_ = myFile.Close()

	d.filePath = filePath
	d.lastReadPos = 0
	return nil
}
