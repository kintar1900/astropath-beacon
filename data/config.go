// Astropath - A Discord Bot for Elite : Dangerous squadrons and communities
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package data

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
)

type Config struct {
	CommanderName string
	ApiToken      string
	JournalPath   string
}

const configFileName = "beacon.config"

func LoadConfig() (Config, error) {
	cfg := &Config{}
	data, err := ioutil.ReadFile(configFileName)
	if err != nil {
		syserr := &os.PathError{}
		if !errors.As(err, &syserr) {
			return *cfg, err
		} else {
			err = nil
		}
	}

	if len(data) != 0 {
		err = json.Unmarshal(data, cfg)
	}

	return *cfg, err
}

func SaveConfig(cfg *Config) error {
	data, err := json.Marshal(cfg)
	if err != nil {
		return err
	}

	return ioutil.WriteFile(configFileName, data, 0777)
}
