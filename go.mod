module gitlab.com/kintar1900/astropath-beacon

go 1.14

require (
	github.com/akavel/rsrc v0.9.0 // indirect
	github.com/lxn/walk v0.0.0-20191128110447-55ccb3a9f5c1
	github.com/lxn/win v0.0.0-20191128105842-2da648fda5b4 // indirect
	github.com/radovskyb/watcher v1.0.7
	gitlab.com/kintar1900/astropath-api v1.1.0
	gitlab.com/kintar1900/elite-journalfile v1.2.0
	gopkg.in/Knetic/govaluate.v3 v3.0.0 // indirect
)
