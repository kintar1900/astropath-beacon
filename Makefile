UPX := $(shell command -v upx 2> /dev/null)
AWSCLI := $(shell command -v aws 2> /dev/null)
ZIP := $(shell command -v zip 2> /dev/null)

.PHONY: clean format

GOROOT := $(shell go env GOROOT)
GOPATH := $(shell go env GOPATH)
GO := $(GOROOT)/bin/go
export GOPATH
export GOROOT

VERSION := $(shell git describe --tags --dirty)

LATEST_RELEASE := $(shell git describe --tags $(shell git rev-list --tags --max-count=1))
MASTER_VERSION := $(shell git describe --tags)

clean:
	rm -rf dist/
	rm -rf build/

build: cmd/main.go
	@echo "Building beacon $(VERSION)"
	GOOS="windows" GOARCH="amd64" $(GO) build -o build/astropath-beacon.exe -ldflags "-X gitlab.com/kintar1900/astropath-beacon.version=$(VERSION)" "gitlab.com/kintar1900/astropath-beacon/cmd"

# Compress the executable, if possible
ifndef UPX
	@echo "\033[1;93mWARNING: \033[0mupx is not available: executable will be larger than necessary"
else
	@echo "Compressing executable"
	upx -q build/astropath-beacon.exe > /dev/null
endif
	@echo "Done building beacon"

package: build
# Lambda requires a zip file
ifndef ZIP
	@echo "\033[1;91mERROR: \033[0mzip is not available"
endif
	@echo "Packaging zip"
	mkdir -p dist/
	mkdir -p ziptemp
	cp build/* ziptemp/
	cd ziptemp && zip ../dist/astropath-beacon-$(VERSION).zip ./*
	rm -rf ziptemp/
	@echo "Done packaging beacon"

cli:
# We have to have the AWS CLI in order to create the s3 bucket and deploy the stack
ifndef AWSCLI
	@echo "\033[1;91mFAILURE:\033[0m aws command line is not installed."
	@exit 1
endif

publish: cli package
	aws s3 cp dist/astropath-beacon-$(VERSION).zip s3://astropath.brotherhoodofthevoid.com/
	aws s3 cp dist/astropath-beacon-$(VERSION).zip s3://astropath.brotherhoodofthevoid.com/astropath-beacon-latest.zip

badges:
	echo "{\"latestRelease\": \"$(LATEST_RELEASE)\", \"masterVersion\": \"$(MASTER_VERSION)\"}" > badge-info.json